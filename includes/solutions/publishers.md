
<div class="card" markdown="1">

<header class="card-heading" markdown="1">
![{{publishers-icon-alt[Publishers icon alt text] Publishers icon}}](/img/png/icon-publishers.png)
{: .card-icon }

## {{publishers-heading[Publishers heading] Publishers}}
</header>

<div class="card-summary" markdown="1">
{{publishers-p1[Publishers paragraph 1] Monetize your adblocking audience}}

{{publishers-p2[Publishers paragraph 2] Learn how to serve Acceptable Ads and find more about getting your website whitelisted}}
</div>

<footer class="card-footer" markdown="1">
[{{publishers-learn-more-button[Publishers learn more button] Learn more}}](solutions/publishers){: .btn-outline-primary }

[{{get-whitelisted-button[Get whitelisted button] Get whitelisted}}](get-whitelisted?type=publishers){: .btn-primary }
</footer>

</div>
