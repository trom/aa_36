
<div class="card" markdown="1">

<header class="card-heading" markdown="1">
![{{publishers-icon-alt[Publishers icon alt text] Publishers icon}}](/img/png/icon-publishers.png)
{: .card-icon }

### {{publishers-heading[Who benefits -> Publishers heading] Publishers }}
</header>

{{publishers-body[Who benefits -> Publishers body] Increase your revenue }}
{: .card-summary }

<footer class="card-footer" markdown="1">
[{{publishers-learn-more[Who benefits -> publishers learn more button] Learn more }}](solutions/publishers){: .btn-outline-primary }

[{{get-whitelisted-button[Get whitelisted button] Get whitelisted}}](get-whitelisted?type=publishers){: .btn-primary }
</footer>

</div>
