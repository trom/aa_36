
<div class="card" markdown="1">

<header class="card-heading" markdown="1">
![{{ad-network-icon-alt[Ad networks icon alt text] Ad Networks icon}}](/img/png/icon-ad-networks.png)
{: .card-icon }

### {{ad-networks-heading[Who benefits -> Ad networks heading] Ad Networks }}
</header>

{{ad-networks-body[Who benefits -> Ad Networks body] Monetize your audience }}
{: .card-summary }

<footer class="card-footer" markdown="1">
[{{ad-networks-learn-more[Who benefits -> ad-networks learn more button] Learn more }}](solutions/ad-networks){: .btn-outline-primary }

[{{get-whitelisted-button[Get whitelisted button] Get whitelisted}}](get-whitelisted?type=ad-networks){: .btn-primary }
</footer>

</div>
