
{{distiction-criteria[Paragraph in "General criteria"] Ads should always be recognizable as ads and distinguishable from all other content (e.g. are not hiding the label, are not misleading users into thinking an ad is part of the primary content). Ads should be clearly marked with the word "advertisement" or its equivalent.}}
