# {{specific-criteria-heading[Specefic criteria heading] Specific criteria}} {: #specific-criteria }

#### {{text-ads[Text in "Specific criteria"] Text ads}} {: .item #text-criteria }

  {{attention-grab["Text ads" description in "Specific criteria"] Text ads designed with excessive use of colors and/or other elements to grab attention are not permitted.}}

#### {{image-ads[Subheadline in "Specific criteria"] Image ads}} {: .item #image-criteria }

{{unobtrusive-image["Image ads" description in "Specific criteria"] Static image ads may qualify as acceptable, according to an evaluation of their unobtrusiveness based on their integration on the webpage.}}

#### {{in-feed-ads[Subheadline in "Specific criteria"] In-feed ads}} {: .item #feed-criteria }

{{feed-list-ads["In-feed ads" description in "Specific criteria"] For ads in lists and feeds, the general criteria differ depending on:}}

##### {{placement-requirement[Bullet point in "Specific criteria"] Placement requirements}} {: #feed-ads-placement }

{{entries-feeds[Text below bullet point in "Specific criteria"] Ads are permitted in between entries and feeds.}}

##### {{size-requirment[Bullet point in "Specific criteria"] Size requirements}} {: #feeds-size-requirement}

{{in-feed-ads-size[Text below bullet point in "Specific criteria"] In-feed ads are permitted to take up more space, as long as they are not substantially larger than other elements in the list or feed.}}

#### {{search-ads[Subheadline in "Specific criteria"] Search ads }} {: .item #search-criteria }

{{search-ads-info["Search ads" description in "Specific criteria"] For search ads - ads displayed following a user-initiated search query - the criteria differ depending on:}}

##### {{size-requirement[Bullet point in "Specific criteria"] Size requirements}} {: #search-ads-size }

{{more-space[Text below bullet point in "Specific criteria"] Search ads are permitted to be larger and take up additional screen space.}}

#### {{no-primary-content[Subheadline in "Specific criteria"] Ads on pages with no primary content }} {: .item #no-content }

{{only-text-ads["Ads on pages with no primary content" description in "Specific criteria"] Only text ads are allowed. For webpages without any primary content (e.g. error or parking pages), the criteria differ depending on:}}

##### {{placement-requirement Placement requirements}} {: #no-content-placement }

{{no-placement-limit[Text below point in "Specific criteria"] No placement limitations.}}

##### {{size-requirement}} {: #no-content-size-requirement }

{{no-size-limit[Text below bullet point in "Specific criteria"] No size limitations.}}

#### {{mobile-acceptable-ads Mobile Ads}} {: .item #mobile-acceptable-ads }

##### {{mobile-acceptable-ads-placement-requirements Placement requirements}} {: #no-content-placement }

- {{mobile-acceptable-ads-placement-requirements-static Static ad types (e.g. 6x1 banner and 1x1 tile ad) are allowed to be placed anywhere on the mobile page.}}
- {{mobile-acceptable-ads-placement-requirements-small Small ads (6x1 banner or smaller) are allowed to be placed as a sticky ad on the bottom of the screen. Other formats are not allowed to stick.}}
- {{mobile-acceptable-ads-placement-requirements-large Large ad types (e.g. native tile ads) are only allowed to be placed under the Primary Content.}} [^4] [^5]

##### {{mobile-acceptable-ads-size-requirements Size requirements}} {: #no-content-placement }

{{mobile-acceptable-ads-size-requirement-paragraph Ads showing on mobile screens are bound to the following size restrictions:}}

- {{mobile-acceptable-ads-size-requirement Ads implemented on the scrollable portion of the webpage must not occupy in total more than 50 percent of the visible portion of the webpage.}} [^6]
- {{mobile-acceptable-ads-size-requirement Ads implemented as a ‘sticky ad’ have a maximum height restriction of 75px (or 15%).}}
- {{mobile-acceptable-ads-size-requirement Below the Primary Content, ads are limited to 100% of the screen space.}} [^7]

##### {{mobile-acceptable-ads-animations Animations}} {: #no-content-placement }

{{mobile-acceptable-ads-animations-paragraph Animations are allowed for the 6x1 ad type when placed as a ‘sticky’ ad on the bottom of the screen. Animations have to comply with the LEAN standard for animations, and a close button or some other closing mechanism must be included.}} [^8]

[^4]: {{large-ad[footnote in "Specific criteria mobile ads"] Large ad: any ad >300px height}}
[^5]: {{primary-content[footnote in "Specific criteria mobile ads"] The 'Primary Content' is defined as (based on <a
href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main">Mozilla's description</a> of the `<main>` HTML element): The Primary Content consists of content that is directly related to, or expands upon the central topic of a document or the central functionality of an application. This content should be unique to the document, excluding any content that is repeated across a set of documents such as sidebars, navigation links, copyright information, site logos, and search forms (unless, of course, the document's main function is a search form).}}
[^6]: {{visible-portion[footnote in "Specific criteria mobile ads"] The visible portion of the webpage is defined as a standard CSS pixel size of <a href="https://drive.google.com/file/d/1JGn_-qdlAqMthA6OFImCdpMH7j3OxIFQ/view">360x512px</a> (Samsung Galaxy S7 with the SBrowser), which is based on the standard viewport of <a href="https://deviceatlas.com/blog/mobile-viewport-size-statistics-2017">360x640px</a>, but with the OS- and browser UI elements deducted.}}
[^7]: {{scroll-past-primary-content[footnote in "Specific criteria mobile ads"] This means that users can scroll past the primary content and an ad unit served after it can take up the whole screen; but following this ad unit, additional ads cannot be implemented.}}
[^8]: {{iab-ad-portfolio[footnote in "Specific criteria mobile ads"] From: <a href="https://www.iab.com/wp-content/uploads/2017/08/IABNewAdPortfolio_FINAL_2017.pdf">IAB New Standard Ad Unit Portfolio (2017).pdf</a>}}

#### {{other-acceptable-ads[Subheadline in "Specific criteria"] Other Acceptable Ads formats? }} {: .item #other-formats }

{{new-format["Other Acceptable Ads formats" text in "Specific criteria"] Are your ads displayed on alternative screens, or are you convinced that you have an innovative Acceptable Ads format which doesn't fit the ads outlined above? <a href="mailto:acceptableads@adblockplus.org">Let us know!</a>}}

*[LEAN]: Light, Encrypted, AdChoices supported, and Non-invasive
