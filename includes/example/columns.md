
<div class="row">
  <div class="col-6">
    <p class="bg-info p-a-sm">1/2 column</p>
  </div>
  <div class="col-6">
    <p class="bg-info p-a-sm">1/2 column</p>
  </div>
</div>
<div class="row">
  <div class="col-4">
    <p class="bg-info p-a-sm">1/3 column</p>
  </div>
  <div class="col-8">
    <p class="bg-info p-a-sm">2/2 column</p>
  </div>
</div>
<div class="row">
  <div class="col-3">
    <p class="bg-info p-a-sm">1/4 column</p>
  </div>
  <div class="col-9">
    <p class="bg-info p-a-sm">3/4 column</p>
  </div>
</div>
