
### Blockquotes

> We want to **make the internet better** for everyone. Purging bad ads is a good start.
>
> <cite>Eyeo Gmbh</cite>


<div class="center m-y-md p-y-sm bg-accent" markdown="1">

> We want to **make the internet better** for everyone. Purging bad ads is a good start.
>
> ---
>
> <cite>Eyeo Gmbh</cite>

</div>
