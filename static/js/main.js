(function(){
  document.addEventListener("DOMContentLoaded", function()
  {
    /**************************************************************************
     * General
     **************************************************************************/

    document.documentElement.classList.add("js");
    document.documentElement.classList.remove("no-js");
    // the class "open" was added just in case it's a no-js state
    document.getElementById("sidebar").classList.remove("open");

    /**************************************************************************
     * Sidebar
     **************************************************************************/

    var sidebarOpen = document.getElementById("sidebar-open");
    var sidebarClose = document.getElementById("sidebar-close");
    var root = document.documentElement;

    sidebarOpen.addEventListener("click", function()
    {
      root.classList.add("open-sidebar");
    }, false);

    sidebarClose.addEventListener("click", function()
    {
      root.classList.remove("open-sidebar");
    }, false);


    /**************************************************************************
     * Youtube Embed
     **************************************************************************/

    function YoutubeEmbed(container)
    {
      this.container = container;
      this.timeClickedVideo = 0;

      this.container.querySelector(".youtube-embed-play")
        .setAttribute("src", "/img/png/video-play.png");

      this.container.querySelector(".youtube-embed-video")
        .addEventListener("click", this._onClick.bind(this), false);
    }

    YoutubeEmbed.prototype._onClick = function(event)
    {
      event.preventDefault();

      if (this.container.classList.contains("show-disclaimer"))
      {
        // Enfore 600ms delay
        var currentTime = new Date().getTime();
        if (currentTime - this.timeClickedVideo < 600) return;

        var image = this.container.querySelector(".youtube-embed-video");

        var video = document.createElement("iframe");
        video.classList.add("youtube-embed-video");
        video.setAttribute("frameborder", "0");
        video.setAttribute("height", "285");
        video.setAttribute("width", "520");
        video.setAttribute("itemprop", "video");
        video.setAttribute("allowfullscreen", "allowfullscreen");
        video.setAttribute("src", image.getAttribute("href"));

        image.parentNode.replaceChild(video, image);

        this.container.classList.remove("show-disclaimer");
      }
      else
      {
        this.container.classList.add("show-disclaimer");
        this.timeClickedVideo = new Date().getTime();
      }
    };

    if (document.querySelector(".youtube-embed"))
      new YoutubeEmbed(document.querySelector(".youtube-embed"));

      /**************************************************************************
       * Header disappears on scroll down and re-appears on scroll up in small screens
       **************************************************************************/

    function initNavbarToggle()
    {
      var sidebarHeight = document.getElementById("sidebar").offsetHeight;
      var scrollHandled = false;
      var lastScrollTop = 0;
      var desktopBreakpoint = 1199;

      // IE9 does not support offsetHeight when element is fixed
      if (!sidebarHeight)
        return;

      window.addEventListener("scroll", (function()
      {
        scrollHandled = false;
      }));

      setInterval(function()
      {
        if (window.innerWidth <= desktopBreakpoint)
        {
          root.classList.add("mobile");
          if (!root.classList.contains("open-sidebar") && !scrollHandled)
          {
            scrollHandled = handleScroll();
          }
        }
        else
        {
          root.classList.remove("mobile");
        }
      }, 250);

      function handleScroll()
      {
        var currentScroll = window.pageYOffset;
        if (currentScroll > lastScrollTop)
        {
          root.classList.add("scrollDown");
        }
        else
        {
          root.classList.remove("scrollDown");
        }
        lastScrollTop = currentScroll;
        return true;
      }
    }

    initNavbarToggle();

  }, false);

}());
