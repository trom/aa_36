title=Blog | Acceptable Ads
description=Keeping you up-to-date with everything related to acceptable ads.
breadcrumb=Blog
custom=1

<style>
.card h3
{
  font-size: 20px;
}

.card img
{
  display: block;
  height: auto;
  max-height: 200px;
  width: auto;
  max-width: 100%;
  margin: 0 auto;
}

@media(min-width: 768px)
{
  .card h3 { height: 70px; }
  .card img { max-height: 100px; }
}
</style>

<div class="container" markdown="1">
# {{blog-heading[Blog Heading] The official Acceptable Ads blog}}

<div class="section" markdown="1">
{{blog-paragraph1[Acceptable Ads Blog Paragraph 1] Stay connected.}}
</div>

</div>

<? include blog/all-posts ?>
