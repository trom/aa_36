title=Welcome to the new Acceptable Ads blog!
description=A welcome post for this blog, the new home for all news about the future of online ads.
featured_img_url=/img/png/blog/acceptable-ads-experience.png
featured_alt=A better experience for everyone
author=Ben Williams
parent=blog/index
committee=false
published_date=2017-03-14
template=blog-entry
breadcrumb=Welcome

Welcome! This is the very first post on a blog that will document the future of online advertising. These electronic pages will be devoted to the future of the Acceptable Ads initiative, including the [Acceptable Ads Certification Tool](https://acceptableads.com/en/tool-certification/), set to launch later this year, the Acceptable Ads Committee, which will take over the standards behind the Acceptable Ads initiative, and more.

We at Adblock Plus/eyeo began the Acceptable Ads initiative in 2011 to keep users in control of their online experience, while assuring that the web stayed fair and profitable.
In the intervening period, that initiative has grown up; and as we prepare to send it out into the wide world, this blog will be your primary spot to learn about its development.

At first this space will have two primary directions – the Acceptable Ads Committee and the Acceptable Ads Certification Tool – but it will surely expand to include other topics and threads. It is important to note that while some of its content will be penned by authors within eyeo, Adblock Plus and other ad blockers who believe in the principle of partial ad blocking, there will likely be additional content from authors across the online ads landscape. For instance, when we hand over control of the Acceptable Ads criteria to an independent committee, that committee’s members will create the content for all posts in the committee-related thread.

How it develops beyond these two early focal points is anyone’s guess. But what we know now is that the amount of ad blockers, who are seeking to shift the balance from scorched-earth, absolutist ad blocking to the balanced approach championed by partial ad blocking, is growing. We also know that mobile ad blocking, huge in parts of Asia, is set to take Europe and North America by storm. So whatever these pages hold, you can be sure that they will be meaningful, insightful and on hopefully already a few steps ahead of what’s coming next.

Talk real soon ;)
