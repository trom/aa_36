title=Best Practices
description=A few best practices of how Acceptable Ads are formatted and implemented
parent=about/index
custom=1

<div class="container section" markdown="1">
  <div class="col-6 expand-on-tablet" markdown="1">

# {{best-practices-heading[Best practices heading] Best Practices}}

{{best-practices-subheading[Best practices subheading] Choose the best formats in the best spots. Be clear and relevant. It’s really that simple. }}
{: .subheading }

{{best-practices-summary[Best practices paragraph] The following guidelines have been taken from successful cases of websites implementing Acceptable Ads. Make your website look awesome and start monetizing good forms of advertising, too. It’s this easy:}}

</div>
 </div>

  <div class="container center" markdown="1">
   <div class="row">
    <div class="section col-4" markdown="1">

![{{static-formats-image-alt[Static formats image alt text] Static formats example image}}](/img/png/best-practice-static-format.png)

### {{use-static-formats-header[Use static formats header] Use static formats}} {: #use-static-formats }

{{use-static-formats-p[Use static formats paragraph] Nobody wants to see an ad that’s screaming for attention and distracting them when they’re trying to consume the content. Animated ads are one of the primary causes of ad blocking.}}

  </div>
  <div class="section col-4" markdown="1">

![{{goode-location-image-alt[Good location image alt text] Good location example image}}](/img/png/best-practice-placement.png)

### {{good-location-header[Choose a good location header] Choose a good location}} {: #choose-good-location }

{{good-location-p[Choose a good location paragraph] Ads perform best when located in the header, in the sidebars or after the content.}}

  </div>
  <div class="section col-4" markdown="1">

![{{be-straightforward-image-alt[Be straightforward image alt text] Good location example image}}](/img/png/best-practice-label.png)

### {{be-straightforward-header[Be straightforward header] Be straightforward, and don’t try to disguise ad content as editorial}} {: #be-straightforward }

{{be-straightforward-p[Be straightforward paragraph] Make your ads recognizable by labeling them as such. With new formats such a native, the line between editorial content and ads has never been so thin!}}

  </div>
 </div>
</div>
