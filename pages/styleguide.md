title=Styleguide
description=New Acceptable Ads website styleguide

<? include example/style ?>

<? include example/masthead ?>

<div class="row">
  <div class="col-6">
    <? include example/headings ?>
  </div>
  <div class="col-6">
    <? include example/inline ?>
    <? include example/foregrounds ?>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <? include example/blockquotes ?>
  </div>
  <div class="col-6">
    <? include example/backgrounds ?>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <? include example/images ?>
  </div>
  <div class="col-6">
    <? include example/alignment ?>
    <? include example/videos ?>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <? include example/ordered-lists ?>
  </div>
  <div class="col-6">
    <? include example/unordered-lists ?>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <? include example/unstyled-lists ?>
  </div>
  <div class="col-6">
    <? include example/definition-lists ?>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <? include example/tables ?>
  </div>
  <div class="col-6">
    <? include example/code ?>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <? include example/form-block ?>
  </div>
  <div class="col-3">
    <? include example/form-inline ?>
  </div>
  <div class="col-3">
    <? include example/buttons ?>
  </div>
</div>


<div class="row cards items">
  <div class="col-3">
    <h3>Link list card</h3>
    <? include example/card-list ?>
  </div>
  <div class="col-3">
    <h3>Inverted card</h3>
    <? include example/card-secondary ?>
  </div>
  <div class="col-3">
    <h3>Centered card</h3>
    <? include example/card-center ?>
  </div>
  <div class="col-3">
    <h3>Card item</h3>
    <? include example/card-item ?>
  </div>
</div>

---

### Columns

<? include example/columns ?>

<? include example/5-card-group ?>

<div class="group group-5 items">
  <? include example/card-item ?>
  <? include example/card-item ?>
  <? include example/card-item ?>
  <? include example/card-item ?>
  <? include example/card-item ?>
</div>  

<? include example/4-card-group ?>

<div class="group group-4 items">
  <? include example/card-item ?>
  <? include example/card-item ?>
  <? include example/card-item ?>
  <? include example/card-item ?>
</div>  

<? include example/3-card-group ?>

<div class="group group-3 items">
  <? include example/card-item ?>
  <? include example/card-item ?>
  <? include example/card-item ?>
</div>  

<? include example/section-secondary ?>
<? include example/section-accent ?>
